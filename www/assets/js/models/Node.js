class Node {
	constructor (value) {
		this.value = value;
		this.parent = null;
		this.cost = 0;
		this.heuristic = 0; 
		this.evaluation = 0;
	}
}

export default Node;