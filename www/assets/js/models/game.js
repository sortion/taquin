import { initPuzzle, performMove, manhattanDistance } from "./taquin";
import { heuristics } from "./heuristics";

import Config from "./config"

class Game {
	constructor(size) {
		this.size = size;
		this.init();
		this.distance = manhattanDistance(this.size, this.current, this.goal);
		this.heuristic = heuristics(Config.heuristics, this.current, this.size);
	}

	get moves() {
		return Math.max(this.history.length - 1, 0);
	}

	revertTo(index) {
		this.current = this.history[index].slice();
		this.history = this.history.slice(0, index);
		this.distance = manhattanDistance(this.size, this.current, this.goal);
		this.heuristic = heuristics(Config.heuristics, this.current, this.size);
	}

	undo() {
		if (this.history.length > 1) {
			this.revertTo(this.history.length - 1);
		}
	}

	move(direction) {
		let nextState = performMove(this.size, this.current, direction);
		if (nextState === null || nextState == this.current) {
			return;
		}
		this.history.push(this.current);
		this.current = nextState;
		this.distance = manhattanDistance(this.size, this.current, this.goal);
		this.heuristic = heuristics(Config.heuristics, this.current, this.size);
	}

	init() {
		this.current = initPuzzle(this.size);
		this.history = [this.current];
		this.goal = Array.from(Array(this.size * this.size).keys());
	}

	reset() {
		this.revertTo(0);
	}
}

export default Game;