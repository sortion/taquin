import "./assets/css/style.css"
import "@fortawesome/fontawesome-free/css/all.css";
import View from "./assets/js/views/board.js"
import Game from "./assets/js/models/game.js"
import { solve } from "./assets/js/models/solve.js"
import Config from "./assets/js/models/config.js"

const params = new Proxy(new URLSearchParams(window.location.search), {
	get: (searchParams, prop) => searchParams.get(prop),
  }
);

let boardSize = params.size || 3;

let game;
initGame(boardSize);

/**
 * Move the cell next to the empty cell in the empty cell, if possible
 * releasing the empty cell in the direction of the key pressed
 * 
 * @param {String} directionKey 
 */
function move(directionKey) {
	// Get the position of the cell to move
	let directionIncrements = {
		"ArrowRight": [0, 1],
		"ArrowLeft": [0, -1],
		"ArrowUp": [-1, 0],
		"ArrowDown": [1, 0]
	}
	let directionIncrement = directionIncrements[directionKey];
	game.move(directionIncrement);
	View.update(game);
}

window.addEventListener("keydown", function (event) {
	if (["ArrowRight", "ArrowLeft", "ArrowUp", "ArrowDown"].includes(event.key)) {
		move(event.key);
	}
	// Undo with Ctrl + Z
	if (event.ctrlKey && event.key === "z") {
		game.undo();
		View.update(game);
	}
});

View.newGameButton.addEventListener("click", function (event) {
	game.init();
	View.update(game);
});

View.resetButton.addEventListener("click", function (event) {
	game.reset();
	View.update(game);
});


View.undoButton.addEventListener("click", function (event) {
	game.undo();
	View.update(game);
});


View.openMenuButton.addEventListener('click', function (event) {
	View.menu.classList.toggle('hidden');
});

View.closeMenuButton.addEventListener('click', function (event) {
	View.menu.classList.add('hidden');
});


View.openHelpButton.addEventListener('click', function (event) {
	View.help.classList.toggle('hidden');
});

View.closeHelpButton.addEventListener('click', function (event) {
	View.help.classList.add('hidden');
});

document.querySelectorAll('.board .cell').forEach(element => {
	element.addEventListener('click', function (event) {
		let clickedIndex = parseInt(element.getAttribute('index'));
		let emptyIndex = game.current.indexOf(boardSize * boardSize - 1);
		let emptyRow = Math.floor(emptyIndex / boardSize);
		let emptyCol = emptyIndex % boardSize;
		let clickedRow = Math.floor(clickedIndex / boardSize);
		let clickedCol = clickedIndex % boardSize;
		let deltaRow = clickedRow - emptyRow;
		let deltaCol = clickedCol - emptyCol;
		if (Math.abs(deltaRow) + Math.abs(deltaCol) === 1) {
			let directionIncrement = [-deltaRow, -deltaCol];
			game.move(directionIncrement);
			View.update(game);
		}
	});
});

View.solveButton.addEventListener('click', function (event) {
	let solution = solve(game.current, game.size);
	if (solution === null) {
		alert("No solution found");
	} else {
		console.log(`Solution found in ${solution.length} steps`);
		View.solutionSteps.innerText = solution.length;
		View.fillHistory(View.solution, solution, game.size);
	}
});

View.menuStartButton.addEventListener('click', function (event) {
	View.menu.classList.add('hidden');
	let size = parseInt(document.getElementsByName('size')[0].value);
	initGame(size);
	event.preventDefault();
    event.stopPropagation();
});

View.menuHeuristicSelectButton.addEventListener('click', function (event) {
	Config.heuristics = Array.prototype.slice.call(View.menuHeuristicCheckboxes).reduce(
		(heuristics, checkbox) => {
			if (checkbox.checked) {
				heuristics.push(checkbox.value);
			}
			return heuristics;
		},
		[]
	)
});

document.querySelectorAll('.button.popup').forEach(element => {
	element.addEventListener('click', function (event) {
		let contentElement = document.getElementById(element.getAttribute('data-popup'));
		contentElement.classList.toggle('hidden');
	});
});


function initGame(size) {
	game = new Game(size);
	// Reset the board
	View.board.innerHTML = "";
	View.createBoard(View.board, game.size);
	View.update(game);
	// Reset the goal board
	View.exampleBoard.innerHTML = "";
	View.createBoard(View.exampleBoard, game.size);
	View.fillBoard(View.exampleBoard, game.size, game.goal);

	View.solutionSteps.innerHTML = "x";
	View.solution.innerHTML = "";
	View.history.innerHTML = "";
}
