"""
Performance Measures

Metrics: expanded nodes, extracted nodes, solution length, 
"""

import os
import random
import timeit

import taquin.heuristics as heuristics
import taquin.taquin as taquin
import taquin.solve as solve
from taquin.Node import Node
from typing import Callable as function

import queue

CSV_FILE = "../data/performances_measures_42_4x4.csv"

def a_star_search(initial_node: Node, expand_function, goal_test_function):
	"""
	A* search algorithm

	:param initial_node: the initial node
	:param expand_node_function: the function to expand a node
	:param goal_test_function: the function to test if a node is a goal
	:return: the goal node if found, None otherwise
	"""
	explored = 0
	expanded = 0
	length = 0
	frontier: queue.PriorityQueue = queue.PriorityQueue()
	frontier.put(initial_node)
	explored: set[list] = set()
	while not frontier.empty():
		node: Node = frontier.get()
		explored.add(tuple(node.value))	
		if goal_test_function(node):
			break
		successors = expand_function(node)
		for successor in successors:
			if tuple(successor.value) not in explored:
				expanded += 1
				frontier.put(successor)
	explored = len(explored)
	length = len(solve.path_from_root(node))
	
	return { "explored": explored, "expanded": expanded, "length": length }

def solve_with_count(initial_taquin, size, heuristic_name):
	def expand_function(node):
		successors = []
		for move in taquin.get_possible_moves(node.value, size):
			successor = Node(move)
			successor.parent = node
			successor.depth = node.depth + 1
			successor.priority = successor.depth + heuristics.heuristic(heuristic_name, move, size)
			successors.append(successor)
		return successors
	
	def goal_test_function(node):
		return taquin.is_goal(node.value)
	
	initial_node = Node(initial_taquin)
	measures = a_star_search(initial_node, expand_function, goal_test_function)
	return measures
	

def init_csv():
	if not os.path.exists(CSV_FILE):
		with open(CSV_FILE, "w") as f:
			f.write("heuristic;n;shuffling_iterations;extracted;expanded;length;taquin\n")

def write_measure(n, heuristic, shuffling_iterations, extracted_nodes_count, expanded_nodes_count, solution_length, taquin_array):
	with open(CSV_FILE, "a") as f:
		taquin_str = "".join([str(i) for i in taquin_array])
		f.write(f"{heuristic};{n};{shuffling_iterations};{extracted_nodes_count};{expanded_nodes_count};{solution_length};{taquin_str}\n")

def make_measures(n, heuristic, shuffling_iterations, taquin_generate):
	measures = solve_with_count(taquin_generate, n, heuristic)
	write_measure(n, heuristic, shuffling_iterations, measures["explored"], measures["expanded"], measures["length"], taquin_generate)
	
def main():
	init_csv()
	n = 4
	shuffling_iterations = 1000
	for heuristic in ["mis", "h6", "tic"]:
		random.seed(12345678)
		for _ in range(30):
			print(heuristic, f"{_} / 10")
			taquin_generate = taquin.generate_random_state(n, shuffling_iterations)
			make_measures(n, heuristic, shuffling_iterations, taquin_generate)

if __name__ == "__main__":
	main()
