"""
Performance Measures

Count solution lenght
"""

import os
import random

import taquin.taquin as taquin
import taquin.heuristics as heuristics
import taquin.search as search
from taquin.Node import Node

def solve_with_count(initial_taquin, size, heuristic_name):
	def expand_function(node):
		successors = []
		for move in taquin.get_possible_moves(node.value, size):
			successor = Node(move)
			successor.parent = node
			successor.depth = node.depth + 1
			successor.heuristic = heuristics.heuristic(heuristic_name, move, size)
			successor.priority = successor.depth + successor.heuristic
			successors.append(successor)
		return successors

	def goal_test_function(node):
		return taquin.is_goal(node.value)
	
	initial_node = Node(initial_taquin)
	leaf_node = search.a_star_search(initial_node, expand_function, goal_test_function)
	if leaf_node is None:
		return None
	else:
		return leaf_node.depth


CSV_FILE = "../data/solutions_length_measures_1.csv"

def init_csv():
	if not os.path.exists(CSV_FILE):
		with open(CSV_FILE, "w") as f:
			f.write("heuristic;n;shuffling_iterations;length\n")

def write_measure(n, heuristic, shuffling_interations, length):
	with open(CSV_FILE, "a") as f:
		f.write(f"{heuristic};{n};{shuffling_interations};{length}\n")


def make_measure(n, heuristic,shuffling_interations):
	return solve_with_count(taquin.generate_random_state(n, shuffling_interations), n, heuristic)

def loop_measure(n,heuristic,shuffling_interations):
	for _ in range(10):
		t = make_measure(n, heuristic,shuffling_interations)
		write_measure(n, heuristic, shuffling_interations, t)

def main():
	init_csv()
	n = 3
	list_heuristics = list(heuristics.HEURISTICS)
	for heuristic in list_heuristics :
		random.seed(42)
		for i in range(50,100,10):
			loop_measure(n,heuristic,i)
		for i in range(100,500,50):
			loop_measure(n,heuristic,i)
		for i in range(500,1000+1,100):
		    loop_measure(n,heuristic,i)

if __name__ == "__main__":
	main()