"""
Performance Measures
"""

import timeit
import os
import random

import taquin.solve as solve
import taquin.taquin as taquin
import taquin.heuristics as heuristics

CSV_FILE = "../data/time_measures_1.csv"

def init_csv():
	if not os.path.exists(CSV_FILE):
		with open(CSV_FILE, "w") as f:
			f.write("heuristic;n;shuffling_interations;time\n")

def write_measure(n, heuristic, shuffling_interations, time):
	with open(CSV_FILE, "a") as f:
		f.write(f"{heuristic};{n};{shuffling_interations};{time}\n")


def make_measure(n, heuristic, shuffling_interations):
	execute = lambda : solve.solve(taquin.generate_random_state(n, shuffling_interations), n, heuristic)
	return timeit.timeit(execute, number=1)

def loop_measure(n,heuristic,shuffling_interations):
	for _ in range(10):
		t = make_measure(n, heuristic,shuffling_interations)
		write_measure(n, heuristic, shuffling_interations, t)

def main():
	init_csv()
	n = 3
	list_heuristics = list(heuristics.HEURISTICS)
	for heuristic in list_heuristics :
		random.seed(42)
		for i in range(50,100,10):
			loop_measure(n,heuristic,i)
		for i in range(100,500,50):
			loop_measure(n,heuristic,i)
		for i in range(500,1000+1,100):
		    loop_measure(n,heuristic,i)

if __name__ == "__main__":
	main()