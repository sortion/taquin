"""
Performance Measures

Count expanded nodes, using modified A* algorithm.
"""

import os
import queue
import random

import taquin.taquin as taquin
import taquin.heuristics as heuristics
import taquin.search as search
import taquin.solve as solve
from taquin.Node import Node

def a_star_search_with_count(initial_node, expand_function, goal_test_function):
	frontier: queue.PriorityQueue = queue.PriorityQueue()
	frontier.put(initial_node)
	explored: set[list] = set()
	while not frontier.empty():
		node: Node = frontier.get()
		explored.add(tuple(node.value))	
		if goal_test_function(node):
			return len(explored)
		successors = expand_function(node)
		for successor in successors:
			if tuple(successor.value) not in explored:
				frontier.put(successor)
	return len(explored)


def solve_with_count(initial_taquin, size, heuristic_name):
	def expand_function(node):
		successors = []
		for move in taquin.get_possible_moves(node.value, size):
			successor = Node(move)
			successor.parent = node
			successor.depth = node.depth + 1
			successor.priority = successor.depth + heuristics.heuristic(heuristic_name, move, size)
			successors.append(successor)
		return successors
	
	def goal_test_function(node):
		return taquin.is_goal(node.value)
	
	initial_node = Node(initial_taquin)
	expanded_nodes_count = a_star_search_with_count(initial_node, expand_function, goal_test_function)
	return expanded_nodes_count
	
CSV_FILE = "../data/expanded_nodes_measures_1.csv"

def init_csv():
	if not os.path.exists(CSV_FILE):
		with open(CSV_FILE, "w") as f:
			f.write("heuristic;n;shuffling_interations;explored\n")

def write_measure(n, heuristic, shuffling_interations, explored):
	with open(CSV_FILE, "a") as f:
		f.write(f"{heuristic};{n};{shuffling_interations};{explored}\n")


def make_measure(n, heuristic, shuffling_interations):
	return solve_with_count(taquin.generate_random_state(n, shuffling_interations), n, heuristic)
	
def loop_measure(n,heuristic,shuffling_interations):
	for _ in range(10):
		t = make_measure(n, heuristic,shuffling_interations)
		write_measure(n, heuristic, shuffling_interations, t)

def main():
	init_csv()
	n = 3
	list_heuristics = list(heuristics.HEURISTICS)
	for heuristic in list_heuristics :
		random.seed(42)
		for i in range(50,100,10):
			loop_measure(n,heuristic,i)
		for i in range(100,500,50):
			loop_measure(n,heuristic,i)
		for i in range(500,1000+1,100):
		    loop_measure(n,heuristic,i)

if __name__ == "__main__":
	main()