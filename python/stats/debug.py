"""
Debug tests
"""

import os
import random

import taquin.taquin as taquin
import taquin.heuristics as heuristics
import taquin.search as search
from taquin.Node import Node
import taquin.solve as solve



def main():
	n = 3
	list_heuristics = list(heuristics.HEURISTICS)
	for i in range(1,50):
		random.seed(i)
		taq = taquin.generate_random_state(n,100)
		r1 = solve.solve(taq,n,list_heuristics[0],"astar")
		r2 = solve.solve(taq,n,list_heuristics[1],"astar")
		if len(r1) != len(r2):
			print("i = ",i,"\nr1\n",r1,"\nr2\n",r2)
		

if __name__ == "__main__":
	main()