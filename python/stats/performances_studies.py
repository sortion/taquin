"""
Performance Measures

Count expanded nodes, using modified A* algorithm.
"""

import os
import random
import timeit

import taquin.heuristics as heuristics
import taquin.taquin as taquin
import taquin.solve as solve

import stats.frontier_extraction as extracted
import stats.expanded_nodes as expanded
import stats.solution_length as length
import stats.data

	
CSV_FILE = "../data/performances_measures_1.csv"

def init_csv():
	if not os.path.exists(CSV_FILE):
		with open(CSV_FILE, "w") as f:
			f.write("heuristic;n;shuffling_interations;extracted;expanded;length;time\n")

def write_measure(n, heuristic, shuffling_interations, extracted_nodes_count, expanded_nodes_count, solution_length, t):
	with open(CSV_FILE, "a") as f:
		f.write(f"{heuristic};{n};{shuffling_interations};{extracted_nodes_count};{expanded_nodes_count};{solution_length};{t}\n")


def make_measures(n, heuristic, shuffling_interations, taquin_generate):
	extracted_nodes_count = extracted.solve_with_count(taquin_generate, n, heuristic)
	expanded_nodes_count = expanded.solve_with_count(taquin_generate, n, heuristic)
	solution_length = length.solve_with_count(taquin_generate, n, heuristic)
	execute = lambda : solve.solve(taquin_generate, n, heuristic)
	t = timeit.timeit(execute, number=1)
	write_measure(n, heuristic, shuffling_interations,extracted_nodes_count, expanded_nodes_count,solution_length,t)
	
def loop_measure(n,heuristic,shuffling_interations):
	for _ in range(10):
		taquin_generate = taquin.generate_random_state(n, shuffling_interations)
		make_measures(n, heuristic, shuffling_interations, taquin_generate)
		

def main():
	init_csv()
	n = 3
	list_heuristics = list(heuristics.HEURISTICS)
	for heuristic in list_heuristics :
		random.seed(42)
		for i in range(50,100,10):
			loop_measure(n,heuristic,i)
		for i in range(100,500,50):
			loop_measure(n,heuristic,i)
		for i in range(500,1000+1,100):
		    loop_measure(n,heuristic,i)

if __name__ == "__main__":
	main()