# Python implementation

## Running the solver

```bash
python3 -m taquin.cli -n 3 -h "pi6"
```
## Running unit tests

```bash
python3 -m taquin.tests.test_heuristics
# change "test_heuristics" with the unit test file from ./taquin/tests
```

