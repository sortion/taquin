import unittest
from .. import taquin

class TestTaquin(unittest.TestCase):
	
	def test_is_goal(self):
		state = [
			0, 1, 2,
			3, 4, 5,
			6, 7, 8
		]
		self.assertTrue(taquin.is_goal(state))

	def test_is_not_goal(self):
		state = [
			1, 2, 3,
			4, 5, 6,
			7, 8, 0
		]
		self.assertFalse(taquin.is_goal(state))

	def test_get_possible_moves(self):
		state = [
			1, 2, 
			3, 0
		]
		possible_moves = list(taquin.get_possible_moves(state, 2))
		for move in possible_moves:
			taquin.print_state(state, 2)
			print("->")
			taquin.print_state(move, 2)
			print()
		self.assertEqual(len(possible_moves), 2)

def main():
	unittest.main()

if __name__ == "__main__":
	main()
