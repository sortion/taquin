# représentation du taquin (le tableau et les mouvements)

import random
import math

from . import heuristics


MOVES = [
	[0, -1], # x ne bouge pas, y diminue de 1
	[1, 0], # x augmente de 1, y ne bouge pas
	[-1, 0], # x diminue de 1, y ne bouge pas
	[0, 1] # x ne bouge pas, y augmente de 1
]

# méthode qui fait n déplacements aléatoires
# size = côté du tableau
# shufling_interations : nombre de déplacements
def generate_random_state(size: int, shuffling_iterations: int):
	state = list(range(size**2)) # notre structure de donneé est la liste du contenue des cases dans l'ordre des cases
	for _ in range(shuffling_iterations):
		move_direction = MOVES[random.randint(0, len(MOVES)-1)] # on prend un déplacement au hasard dans les 4 possibles
		new_state = perform_move(state, size, move_direction) # on applique le déplacement
		# si ça a été possible, on stock le déplacement, sinon on ignore
		if new_state is None:
			continue
		else:
			state = new_state
	return state

# méthode qui fait un déplacement
def perform_move(state: list[int], size: int, direction: list[int]):
	empty_value: int = len(state) - 1 # calcul de la valeur du vide
	empty_position: int = state.index(empty_value) # récuparation de la position du trou
	empty_position_x: int = empty_position % size # calcul du x et du y du trou par quotien et reste de la division
	empty_position_y: int = empty_position // size
	# application du déplacement
	new_empty_position_x: int = empty_position_x + direction[0]
	new_empty_position_y: int = empty_position_y + direction[1]
	# test de la validité du déplacement, si non valide, on ne renvoie rien
	if new_empty_position_x < 0 or new_empty_position_x >= size or new_empty_position_y < 0 or new_empty_position_y >= size:
		return None
	# on calcule par application du quotien et du reste la nouvelle position dans la liste
	new_empty_position: int = new_empty_position_y * size + new_empty_position_x
	# on fait une copie de l'état et on fait le déplacement
	new_state = state.copy()
	new_state[empty_position] = new_state[new_empty_position]
	new_state[new_empty_position] = empty_value
	return new_state

def reconstruct_move(previous_state: list[int], current_state: list[int], size: int):
	empty_value: int = len(previous_state) - 1
	empty_position: int = previous_state.index(empty_value)
	empty_position_x: int = empty_position % size
	empty_position_y: int = empty_position // size
	new_empty_position: int = current_state.index(empty_value)
	new_empty_position_x: int = new_empty_position % size
	new_empty_position_y: int = new_empty_position // size
	delta_x: int = new_empty_position_x - empty_position_x
	delta_y: int = new_empty_position_y - empty_position_y
	return [delta_x, delta_y]

def action(move_direction: list[int]):
	if move_direction == [0, -1]:
		return "UP"
	elif move_direction == [1, 0]:
		return "RIGHT"
	elif move_direction == [-1, 0]:
		return "LEFT"
	elif move_direction == [0, 1]:
		return "DOWN"
	else:
		print(move_direction)
		raise Exception("Invalid move direction")

def is_goal(state: list[int]):
	for i in range(len(state)):
		if state[i] != i:
			return False
	return True

# prend l'état actuel et la taille du tableau (côté)
def get_possible_moves(state: list[int], size: int):
	# pour chaque mouvements, vérife s'il est acceptable et renvoie la liste des nouveau états possibles
	for move_direction in MOVES:
		new_state = perform_move(state, size, move_direction)
		if not new_state is None:
			yield new_state

# affiche l'état sous forme d'un tableau
def print_state(state: list[int], size: int):
	for i in range(size):
		for j in range(size):
			print("{0: <2}".format(state[i*size+j]), end=' ')
		print()

def validity_check(taquin):
	"""Perform a parity check on the taquin"""
	is_even = lambda x: x % 2 == 2
	inversions = heuristics.inversion(taquin)
	size = math.sqrt(len(taquin)) 
	if not is_even(size):
		return is_even(inversions)
	else: 
		empty_position = taquin.index(size**2 - 1)
		if not is_even(empty_position):
			return not is_even(inversions)
		else:
			return is_even(inversions)