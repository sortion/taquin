"""Heuristiques - Jeu du Taquin pour un carré 3 par 3 et généralisé à n x n"""

import math

HEURISTICS = {
	"h1": {
		"pi": [36, 12, 12, 4, 1, 1, 4, 1, 0],
		"rho": 4
	},
	"h2": {
		"pi": [8, 7, 6, 5, 4, 3, 2, 1, 0],
		"rho": 1
	},
	"h3": {
		"pi": [8, 7, 6, 5, 4, 3, 2, 1, 0],
		"rho": 4
	},
	"h4": {
		"pi": [8, 7, 6, 5, 3, 2, 4, 1, 0],
		"rho": 1
	},
	"h5": {
		"pi": [8, 7, 6, 5, 3, 2, 4, 1, 0],
		"rho": 4
	},
	"h6": {
	},
	"mis": {
	},
	"hic": {
	},
	"lic": {
	},
	"tic": {
	}
}

def misplaced_tiles(state, size):
	"""Number of misplaced tiles"""
	counter = 0
	for i in range(len(state)):
		if state[i] != i:
			counter += 1
	return counter

def heuristic_h6(state, size):
	return sum([
		manhattan_distance(i, state, size) for i in range(len(state) - 1)
	])

def heuristic_hic(state, size):
	"""An attempt to implement a heuristic for n x n grids"""
	def weight(index):
		return index
	return sum([
		weight(i) * manhattan_distance(i, state, size) for i in range(len(state) - 1)
	])

def heuristic_lic(state, size):
	"""Manhattan Distance + Linear Conflicts Heuristic"""
	return sum([
		manhattan_distance(i, state, size) for i in range(len(state) - 1)
	]) + 2 * linear_conflicts(state, size)

def heuristic_tic(state, size):
	"""
	Manhattan distance - Well-orneriness
	"""
	well_ordered_counter = 0
	for i in range(len(state)):
		if state[i] == i:
			well_ordered_counter += 1
		else:
			break
	return sum([
		manhattan_distance(i, state, size) for i in range(len(state) - 1)
	]) - well_ordered_counter


def heuristic(name, state, size):
	if name == "hic":
		return heuristic_hic(state, size)
	if name == "mis":
		return misplaced_tiles(state, size)
	if name == "lic":
		return heuristic_lic(state, size)
	if name == "tic":
		return heuristic_tic(state, size)
	if name == "h6":
		return heuristic_h6(state, size)
	pi = HEURISTICS[name]["pi"]
	rho = HEURISTICS[name]["rho"]
	return sum([
		pi[i] * manhattan_distance(i, state, size) for i in range(len(state))
	]) // rho

def manhattan_distance(index, state, size):
	value = state[index]	
	current_x = index % size
	current_y = index // size
	target_x = value % size
	target_y = value // size
	return abs(target_x - current_x) + abs(target_y - current_y)

def inversion(taquin):
	"""Number of inversions
	
	Let a, b be two elements of the taquin.
	There is an inversion if a > b and a is on the left of b.
	"""
	counter = 0
	for i in range(len(taquin)):
		for j in range(i + 1, len(taquin)):
			if taquin[i] > taquin[j]:
				counter += 1
	return counter

def linear_conflicts(taquin, size):
	"""Number of linear conflicts
	
	A linear conflict occurs between tiles ta and tb when ta and tb are both on the same line, 
	and the goal positions of ta is to the right of the goal position of tb,
	"""
	counter = 0
	# For each row and each column
	for i in range(size):
		# For each pair (i, j) of tiles
		for j in range(size - 1):
			for k in range(j+1, size):
				# Row
				# If there is a conflict
				if taquin[i * size + j] > taquin[i * size + k]:
					counter += 1
				# Column
				if taquin[j * size + i] > taquin[k * size + i]:
					counter += 1
	return counter
