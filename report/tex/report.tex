\documentclass[
	fleqn,
	oneside,
	11pt
]{chameleon/sty/rep}

\title{Le jeu du Taquin}
\author{Naïa PÉRINELLE (20200653) \\ Samuel ORTION (20200440)}
\subtitle{Mini Projet n°2}
\subject{Intelligence Artificielle}
\affiliation{Université d'Évry Val d'Essonne -- Université Paris-Saclay}
\definecolor{title}{named}{green}
\date{6 Avril 2023}
\usepackage{polyglossia}
\setmainlanguage{french}

\input{preamble}

\begin{document}

\makecover

\chapter{Le taquin}

Le jeu du Taquin est une grille $n \times n$ remplie des chiffres de 0 à $n^2 - 1$, la tuile $n^2 - 1$ est remplacée par un tuile vide. À partir d'une grille mélangée, l'objectif est d'obtenir la grille avec les chiffres de 0 à $n^2 - 1$, et la tuile vide sur la dernière case. Pour parvenir à une solution, seuls les déplacements de la tuile vide sont autorisés.

\begin{table}[H]
    \centering
     \begin{subtable}[a]{0.3\textwidth}
        \centering
        \begin{tabular}{|c|c|c|}
            \hline
            2 & 5 & 0 \\ 
            \hline
            6 & 3 & x \\ 
            \hline
            7 & 1 & 4  \\
            \hline
        \end{tabular}
        \caption{L'état initial}
    \end{subtable}
    \begin{subtable}[a]{0.3\textwidth}
        \centering
        \begin{tabular}{|c|c|c|}
           \hline
           0 & 1 & 2 \\
           \hline
           3 & 4 & 5 \\
           \hline  
           6 & 7 & x \\
           \hline
        \end{tabular}
        \caption{L'état du but}
    \end{subtable}
    \caption{Un exemple de problème de Taquin $3 \times 3$ et la configuration à atteindre.}
    \label{fig:taquin-3x3}
\end{table}

Le code source de ce projet est disponible sur \url{https://framagit.org/UncleSamulus/taquin/}

\chapter{Recherche de solution}

\section{Algorithmes de recherche}

Pour rechercher une solution au Taquin, plusieurs algorithmes de recherche peuvent être utilisés.
Nous avons commencé par implémenter la recherche avec l'algorithme $A^*$.
Nous avons aussi implémenté l'algorithme $IDA^*$ \textit{Iterative Deepening $A^*$}, algorithme de recherche en profondeur d'abord, qui favorise les branches de l'arbre les plus prometteuses.

$A^*$ comme $IDA^*$ se basent sur des heuristiques pour estimer le coût du chemin de l'état courant jusqu'à l'état du but.
Ce faisant, ces deux algorithmes réduisent la taille de l'arbre de recherche, tout en assurant de retourner une solution optimale, tant que l'heuristique est admissible.

\begin{algorithm}[H]
\begin{algorithmic}[1]
\Procedure{AStar-Search}{$s$, $goal$}
\State $\textit{frontière} \gets \{ s \}$ \Comment{File de priorité des états à expanser, ordonnée par ordre croissant de $estimate$} 
\State $\textit{exploré} \gets \emptyset$ \Comment{Ensemble des états déjà explorés}
\Loop
\If {$\textit{frontière} = \emptyset$}
\State \Return \texttt{fail}
\EndIf
\State $s \gets \textit{frontière}.remove()$ \Comment{$s$ devient le prochain état de la frontière qui a la valeur $f$ la plus petite}
\If {$s = goal$}
\State \Return \texttt{solution}
\EndIf
\State $\textit{exploré} \gets \textit{exploré} \cup \{n\}$
\For {$n \in \Call{Successors}{s}$}
    \If{$, \notin \textit{exploré}$}
    \State $n.estimate \gets n.depth + n.heuristic$
    \State $\textit{frontière}.insert(n)$
    \EndIf
\EndFor
\EndLoop
\EndProcedure
\end{algorithmic}
\caption{Algorithme de Recherche $A^*$}
\end{algorithm}

\begin{algorithm}[H]
    \begin{algorithmic}[1]
        \Procedure{IterativeDeepeningAStar}{$s$, $goal$}
        \State $bound \gets s.heuristic$ \Comment{Limite supérieure de profondeur au delà de laquelle la recherche est stoppée}
        \State $path = []$
        \Loop
            \State $result \gets \Call{Search}{s, path, bound}$
            \If{$result.found$}
            \State \Return $result.node$
            \EndIf
            \If{$result.estimate = \infty$}
            \State \Return \texttt{fail}
            \EndIf
            \State $bound \gets result.estimate$
        \EndLoop
        \EndProcedure

        \Procedure{Search}{$s$, $path$, $bound$} 
        \State $estimate \gets s.depth + s.heuristic$ \Comment{$estimate$ correspond à $f = g + h$, où $g$ est le coût d'une solution et $h$ une estimation du chemin qui reste à parcourir}
        \If{$estimate > bound$}
            \State \Return $\{found: \texttt{false}, node: s, estimate\}$
        \EndIf
        \If {$s = goal$} 
            \State \Return $\{found: \texttt{true}, node: s, estimate\}$
       \EndIf
        \State $m \gets \infty$
        \For {$n \in \Call{Successors}{s}$} 
            \If {$n \notin path$} 
                \State $result \gets \Call{Search}{n, path \cup \{ s \}, bound}$
                \If {$result.found$}
                    \State \Return $result$
                \EndIf
                \If {$result.estimate < m$} 
                    \State $m \gets result.estimate$
                \EndIf
            \EndIf
        \EndFor
        \State \Return $\{found: \texttt{false}, node: s, estimate\}$
        \EndProcedure
        \end{algorithmic}
    \caption{Algorithme de recherche $IDA^*$}
\end{algorithm}

\section{Heuristiques}

\subsection{Des heuristiques pour le taquin $n = 3$}

Pour le taquin $3 \times 3$, les 6 heuristiques $h_1 (\cdot), h_2(\cdot), \dots, h_6(\cdot)$ sont définies comme des distances pondérées réduites entre l'état courant $E$ et l'état final, comme estimation de la longueur du chemin restant à parcourir: pour $1 \leq k \leq 6$,
\[
    h_k (E) = \left( \sum_{i=0}^8 \pi_k (i) \times \varepsilon_E(i) \right) // \rho_k.
\]

\begin{enumerate}
    \item 
$\varepsilon_E(i)$ est la distance de Manhattan entre la case $i$ portant le nombre $n$ à l'état $E$, comparée à la case de $n$ dans l'état du but:
\[
    \varepsilon_E(i) = |x_{current} - x_{target}| + |y_{current} - y_{target}|
\]
où $(x_{current}, y_{current})$ (resp. $(x_{target}, y_{target})$) sont les coordonnées de la case $i$ et les coordonnées du nombre porté par $i$ dans l'état du but respectivement.

\item $\pi_k(i)$ est le poids de la tuile $i$;
\item $\rho_k$ est un coefficient de normalisation.

\end{enumerate}

\begin{figure}
    \centering
    \includegraphics{media/scheme/manhattan.png}
    \caption{Un schéma pour la distance de Manhattan. Les chemins rouges, bleu et jaune correspond à des distances de Manhattan, le chemin vert à la distance euclidienne.}
    \label{fig:scheme-manhattan}
\end{figure}

\begin{table}[H]
    \centering
    \begin{tabular}{ccccccccccc}
    \toprule
        Tuile & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & $X$ & $\rho_k$ \\
        \midrule
        $h_1$ & 36 & 12 & 12 & 4 & 1 & 1 & 4 & 1 & 0 & 4 \\
        $h_2$ & 8 & 7 & 6 & 5 & 4 & 3 & 2 & 1 & 0 & 1 \\
        $h_3$ & 8 & 7 & 6 & 5 & 4 & 3 & 2 & 1 & 0 & 4 \\
        $h_4$ & 8 & 7 & 6 & 5 & 3 & 2 & 4 & 1 & 0 & 1 \\
        $h_5$ & 8 & 7 & 6 & 5 & 3 & 2 & 4 & 1 & 0 & 4 \\
        $h_6$ & 1 & 1 & 1 & 1 & 1 & 1 & 1 & 1 & 0 & 1 \\
    \bottomrule
    \end{tabular}
    \caption{Récapitulatif des poids et des coefficients de normalisation des heuristiques 1 à 6}
    \label{tab:heuristiques}
\end{table}

\subsection{Des heuristiques pour $n$ quelconque}

Un problème avec les heuristiques $h_k$ pour $k = 1, \dots, 5$, c'est qu'elles ne sont valables que pour $n \leq 3$.

Pour palier à ce problème, et explorer davantage le champs des heuristiques, nous introduisons d'autres heuristiques $h$ telles que définies ci-dessous:

\paragraph{$h_{hic}$} L'heuristique $h_{hic}$ essaie de pondérer astucieusement les distance de manhattan pour privilégier la résolution case à case.
\[
h_{hic} = \left(\sum_{i=0}^{n^2 - 2} w(i) * \varepsilon_E(i) \right)
\]
Avec
\[
w(i) = i
\]

\paragraph{$h_6$ Distance de Manhattan} L'heuristique $h_6$ consiste en la somme des distances de manhattan de chacune  des tuiles. Cette heuristique est aisément généralisée pour $n$ quelconque.

On note que cette heuristique répond aux deux critères d'admissibilité :
\begin{itemize}
    \item pour arriver à la solution, chaque tuile devra être déplacée au moins de la distance de manhatan entre sa position de départ et celle de destination, et très probablement davantage vus les contraintes, donc cette distance est un minorant de la distance restant à parcourir dans la solution optimale
    \item pour chaque mouvement réalisé, soit on réduit de 1 la distance de manhatan d'une tuile, et on a égalité $h(n) = c(n, a, n') + h(n')$, soit on ne réduit pas de 1 et donc on a $h(n) < c(n, a, n') + h(n')$
\end{itemize}
Donc cette heuristique doit donner un résultat optimal avec l'algorithme A*.

\paragraph{$h_{mis}$}  L'heuristique $h_{mis}$ compte le nombre de tuiles mal placées.

\paragraph{$h_{tic}$} L'heuristique $h_{tic}$ part de la constatation que la méthode de résolution des taquin par un humain consiste par exemple à placer les cases aux bons endroits, une à une, en partant d'un coin en haut à gauche. L'idée est donc de favoriser la sélection des états où les cases sont bien placées dans l'ordre, en soustrayant à l'heuristique le nombre de cases bien placées les unes à la suite des autres.

\paragraph{$h_{lic}$} Cette heuristique compte la distance de Manhattan plus le nombre de conflits en ligne. Un conflit en ligne apparaît quand deux tuiles $a$ et $b$ sont sur la même ligne et $a < b$ alors que $b$ est avant $a$ sur la ligne. On ajoute 2 fois le nombre de conflits à l'heuristique, en partant du principe qu'il faut \textit{a minima} deux coups pour résoudre ce conflit.

\chapter{Choix d'implémentation}

\section{Structures de données}

\paragraph{Frontière} La frontière est implémentée avec une file de priorité utilisant la classe python \mintinline{python}{queue.PriorityQueue} de la librairie standard. Des objets avec l'attribut \texttt{priority} associé associé à la valeur $g + h$ sont ajouté à cette structure.

\paragraph{Ensemble exploré} L'ensemble exploré utilise l'objet \mintinline{python}{set}, qui repose sur une table de hachage sous le capot. Le test d'appartenance à l'ensemble est en $O(1)$ en principe (en omettant le coût de la comparaison de deux éléments).

\paragraph{Arbre de recherche} Au cours de l'exécution les nœuds successifs enregistrent les nœuds qui les précèdent. Cet objet enregistre la valeur de l'heuristique calculée pour ce nœud, son coût (c'est-à-dire sa profondeur dans l'arbre de recherche), et l'état du taquin.


\chapter{Étude des performances}

Dans ce chapitre, vont être comparés les heuristiques du point de vue de la performance dans le but de déterminer s'il y a des heuristiques meilleurs que d'autres.



\section{Temps CPU}

Le temps d'exécution peut varier selon des paramètres externes au programme, tel que la puissance de l'ordinateur, sa RAM, l'exécution parallèle de d'autres programmes. Ces variables rendent la mesure du temps CPU plus délicate à étudier

Il est donc intéressant de chercher d'autres moyen d'étudier le temps d'exécution. Une hypothèse est que le temps est fonction du nombre d'états explorés.

La figure ci-dessous présente pour toutes les heuristiques confondues, et pour $n=3$, le temps en fonction du nombre d'états explorés. On note alors que le nuage de points suit une fonction linéaire. On peut voir sur cette figure deux droites se distinguer. On pourrait penser qu'il s'agit simplement de l'intervention d'une variable extérieure comme par exemple une mise à jour en arrière plan. Une autre hypothèse pourrait être que le nombre d'opérations faîtes pour chaque état exploré diffère de façon notable entre deux familles d'heuristiques.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{media/plot/n=3/f(expanded) = time.png}
    \caption{La mesure du temps d'exécution en fonction du nombre d'états explorés, pour des taquin $3 \times 3$}
    \label{fig:f(expanded)=time}
\end{figure}

Pour la suite, on peut considérer qu'étudier le nombre de noeuds expansés revient à étudier le temps d'exécution.

\section{Nombre de nœuds expansés}

On étudie la figure ci-dessous qui présente des boites à moustaches du nombre d'états explorés pour chaque heuristique.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{media/plot/n=3/f(heuristique) = expansed.png}
    \caption{Le nombre de noeuds expansés en fonction de l'heuristique}
    \label{fig:f(heuristique) = expansed}
\end{figure}

On note que l'heuristique $hic$ est extrêmement rapide par rapport aux autres. $h_2$ et $h_4$ rendent un résultat très rapidement. $h_3$, $h_5$ et $h_6$ sont assez rapide également et ont l'air d'avoir à peu près le même profil de temps d'exécution.
$h_1$ et $mis$ ont des temps d'exécution en moyenne beaucoup plus élevés que les autres. Cependant, la dispersion et la moyenne de $mis$ sont meilleurs que ceux de $h_1$. En conséquence, et sachant que $h_6$ donne un résultat optimal, on peut considérer que $h_1$ et $mis$ ne sont pas de bonnes heuristiques pour le critère du temps d'exécution.

On peut également étudier le nombre de noeuds expansés en fonction du taquin initial. La figure ci-dessous présente le nombre de noeuds expansés en fonction du nombre de mouvements du mélange (partant de l'état final pour arriver à un état initial valide aléatoire).

\begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{media/plot/n=3/f(shuffling) = expansed.png}
    \caption{Le nombre de noeuds expansés en fonction du nombre de mouvements du mélange}
    \label{fig:f(shuffling) = expansed}
\end{figure}

Entre 50 et 350, la difficulté du problème augmente. Au delà de 350, on observe des variations qui resteraient à expliquer (notamment à 700 et 800 coups de mélange où les écarts-types sont pour l'un très petit pour l'autre très grand) mais, globalement, la difficulté de résolution ne semble plus augmenter significativement. On peut en déduire qu'il ne semble pas nécessaire d'aller au delà de 1000 mouvements de mélange pour un taquin $3 \times 3$.

\section{Nombre d'états sortis de la frontière}

La figure ci-dessous (fig. \ref{fig:f(extracted)=expansed}) montre que pour toutes les heuristiques et quelque soit les taquins initiaux, étudier la performance en fonction du nombre d'états explorés ou du nombre d'états sortis de la frontière revient au même.
L'équation de la droite linéaire $y = 0,8x + 187$ indique qu'il y a, de manière non significative, moins d'états expansés que d'états sortis de la frontière. La différence entre les deux valeurs s'explique par le fait que des états peuvent avoir été placés en doublons dans la frontière, et donc sortis plusieurs fois, alors que le nombre des états explorés ne compte pas de doublons.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{media/plot/n=3/f(extracted) = expansed.png}
    \caption{Le nombre d'états explorés en fonction du nombre sortis de la frontière}
    \label{fig:f(extracted)=expansed}
\end{figure}

Les deux figures ci-dessous (\ref{fig:extracted}) reprennent le nombre d'états sortis de la frontière en fonction soit de l'heuristique soit du nombre de mouvement du mélange. On note qu'elles représentent les mêmes résultats que les figures vues précédemment dans l'étude du nombre d'états expansés, ce qui confirme qu'il n'est pas nécessaire d'étudier l'un après avoir déjà étudié l'autre.

\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{media/plot/n=3/f(heuristique) = extracted.png}
        \caption{$f(heuristique) = extracted$}
        \label{fig:f(heuristique) = extracted}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{media/plot/n=3/f(shuffling) = extracted.png}
        \caption{$f(shuffling) = extracted$}
        \label{fig:f(shuffling) = extracted}
    \end{subfigure}
    \caption{Nombre d'états sortis de la frontière en fonction de l'heuristique ou de nombre de mouvements du mélange}
    \label{fig:extracted}
\end{figure}


\section{Longueur de la solution (Optimalité)}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{media/plot/n=3/f(heuristique) = length.png}
    \caption{f(heuristique) = length}
    \label{fig:f(heuristique) = length}
\end{figure}
$mis$, $h_5$ et $h_3$ donnent des solutions proches de l'optimale alors que les autres, $h_1$, $h_2$, $h_4$ et $hic$ s'écartent de l'optimale. Ces résultats sont associé à l'admissibilité des heuristiques: les heuristiques admissibles que sont par exemple la distance de Manhattan ou le nombre de tuiles mal placées assure que l'algorithme de recherche $A^*$ retourne la solution optimale.

\section{Comparaison d'optimalité et de temps pour chaque heuristique avec $h_6$}

Pour comparer les heuristiques, nous avons deux critères: 
\begin{itemize}
    \item optimalité des solutions trouvées,
    \item temps pour les trouver.
\end{itemize}
Dans les graphes ci-dessous, pour chaque heuristique on construit un diagramme qui donne l'un en fonction de l'autre en comparaison à h6, choisi comme référence car optimal. 

Une concentration de points vers la droite et proche de la ligne horizontale $y=1$ est la marque d'une bonne heuristique. Une heuristique ne donnant pas le résultat optimal n'est potentiellement intéressante que si elle est bien plus rapide que $h_6$ ou $mis$.

\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{media/plot/n=3/comp h6 vs h1.png}
        \caption{$h_1$}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{media/plot/n=3/comp h6 vs h2.png}
        \caption{$h_2$}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{media/plot/n=3/comp h6 vs h3.png}
        \caption{$h_3$}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{media/plot/n=3/comp h6 vs h4.png}
        \caption{$h_4$}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{media/plot/n=3/comp h6 vs h5.png}
        \caption{$h_5$}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{media/plot/n=3/comp h6 vs mis.png}
        \caption{$mis$}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{media/plot/n=3/comp h6 vs hic.png}
        \caption{$hic$}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{media/plot/n=3/comp h6 vs hic zoom.png}
        \caption{$hic zoom$}
    \end{subfigure}
    \caption{Comparaison de l'optimalité et du temps d'une heuristique $h_x$ avec $h_6$}
    \label{fig:my_label}
\end{figure}

Les quelques points situés sous la barre $y=1$ nécessiteraient une étude approfondie car ils ne sont pas censés exister.

Ces graphiques montrent que les heuristiques $h_1$, $h_3$ et $h_5$ sont peu intéressantes : trop éloignées de l'optimalité et/ou trop lentes.

Les heuristique $h_2$ et $h_4$ sont a peu près équivalentes. Elles apportent de meilleurs performances en échange d'un moins bon résultat. Elles pourraient donc être intéressantes.

L'heuristique $mis$ présente des longueurs de solution optimales, mais elle est bien plus lente à exécuter que l'heuristique $h_6$. En conséquence, ont préférera l'heuristique $h_6$.

L'heuristique $hic$ est probablement la meilleure heuristique à utiliser si $h_6$ ne s'exécute pas dans un temps raisonnable.


\section{Quelques mesures pour les taquin $4 \times 4$}

\begin{table}[H]
    \centering
    \begin{tabular}{cccc}
    \toprule
    h & longueur solution  & nœuds expansés  & nœuds extraits de la frontière \\
    \midrule    
        $hic$ & $114$ & $2\,787\,395$ & $1\,412\,883$ \\
        $lic$ & 59 &  $2\,379\,143$ & $1\,218\,462$ \\
        $tic$ & 53 & $5\,230\,541$ & $2\,572\,955$ \\
    \bottomrule
    \end{tabular}
    \caption{Mesures moyennes sur 40 instances de 15-taquins indépendants résolus à l'aide des heuristiques $h_{hic}$, $h_{lic}$ et $h_{tic}$}
    \label{tab:4x4-data}
\end{table}

La table \ref{tab:4x4-data} rapporte quelques mesures moyennes pour la résolution des taquins $4 \times 4$ utilisant certaines des heuristiques pour $n$ quelconque présentées précédemment.

L'heuristique $h_{hic}$ semble retourner une solution assez peu optimale, $h_{lic}$ comme $h_{tic}$ retourne une solution qui semble plus proche de l'optimal. Le nombre de nœuds expansés et de nœuds extraits de la frontière commence à être particulièrement grand. Pour les heuristiques $h_{mis}$ ce nombre est si important que la mesure n'a pas pu être réalisée faute de mémoire RAM. Dans ce type de cas, il aurait été intéressant de pousser plus avant l'exploration de l'algorithme de recherche $IDA^*$, qui limite la consommation de mémoire par sa recherche en profondeur d'abord.

\chapter{Extensions}

\section{Interface web}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{media/interface.png}
    \caption{Un aperçu de l'interface web du Taquin.}
    \label{fig:web-interface}
\end{figure}

Nous avons développé une application web où l'on peut jouer au taquin avec une taille arbitraire de plateau \footnote{\url{https://unclesamulus.frama.io/taquin/}}.

L'algorithme de recherche de solution a été implémenté en JavaScript. Il est utilisable depuis l'interface, mais tend à faire planter le navigateur pour certaines instances de Taquin.

\chapter{Annexe}
\section{Le script python}
\inputminted{python}{code/all.py}

\end{document}